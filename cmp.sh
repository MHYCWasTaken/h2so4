clear
echo "compile..."
g++ -std=c++17 -D_GLIBCXX_USE_CXX11_ABI=0 5x5.cpp -o 5x5.o -lssl -lcrypto
echo "---run---"
./5x5.o
echo "\n"
echo "---end---"
rm 5x5.o