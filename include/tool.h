#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

#ifndef __TOOL_H__

#define __TOOL_H__
vector<string> split(string s, char c);
int to_digit(string s);

#endif