//#include "tool.h"
#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

vector<string> split(string s, char c) {
	vector<string> v;
	string buf = "";
	for (int i = 0; i < s.size(); i++) {
		if (s[i] == c) {
			v.push_back(buf);
			buf = "";
		}
		else {
			buf += s[i];
		}
	}
	return v;
}

int to_digit(string s) {
	int r = 0;
	for (int i = 0; i < s.size(); i++) {
		r *= 10;
		r += s[i] - '0';
	}
	return r;
}


