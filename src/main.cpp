#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "httplib.h"
#include <iostream>
#include <vector>
#include <string.h>
#include <map>
#include "tool.h"
#include "md5.h"
using namespace std;



struct Puzzle;
class Map;
Puzzle get5x5();
string submit(Puzzle p, Map m);
void hallSubmit(string solparams);
void dfs(int t, Map m, map<string, string>& bru);


struct Puzzle {
	string task;
	string hash;
	string param;
};

class Map {
public:
	bool con[5][5];
	bool yn[5][5];
	Map() {
		memset(con, 0, sizeof(con));
		memset(yn, 0, sizeof(yn));
	}
	string clue[2][5];

	void printm();
	bool check(string task);
	string serialize();
	string getClueCol(int t);
	string getClueRow(int t);
	void setConfrim(int i, int j, bool con);
	void setBlock(int i, int j, bool yn);
	void makeClue();
	string getClue();
};

void Map::printm() {
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			if (con[i][j]) {
				cout << yn[i][j] ? "y" : "n";
			}
			else {
				cout << " ";
			}
		}
		cout << ";" << endl;
	}
}

bool Map::check(string task) {
	return getClue() == task;
}

string Map::serialize() {
	string t = "";
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			t += (yn[i][j] ? 'y' : 'n');
		}
	}
	return t;
}

string Map::getClueCol(int t) {
	vector<int> stat;
	int goon = 0;
	for (int i = 0; i < 5; i++) {
		if (yn[i][t]) {
			goon++;
		}
		else {
			if (goon != 0) stat.push_back(goon);
			goon = 0;
		}
	}
	if (goon != 0) stat.push_back(goon);
	string buff = "";
	for (int i = 0; i < stat.size(); i++) {
		buff += (stat[i] + '0');
		buff += '.';
	}
	return buff.substr(0, buff.size() - 1);
}

string Map::getClueRow(int t) {
	vector<int> stat;
	int goon = 0;
	for (int i = 0; i < 5; i++) {
		if (yn[t][i]) {
			goon++;
		}
		else {
			if (goon != 0) stat.push_back(goon);
			goon = 0;
		}
	}
	if (goon != 0) stat.push_back(goon);
	string buff = "";
	for (int i = 0; i < stat.size(); i++) {
		buff += (stat[i] + '0');
		buff += '.';
	}
	return buff.substr(0, buff.size() - 1);
}

void Map::setConfrim(int i, int j, bool conn) {
	con[i][j] = conn;
}

void Map::setBlock(int i, int j, bool ynn) {
	yn[i][j] = ynn;
	clue[1][i] = getClueRow(i);
	clue[0][j] = getClueCol(j);
}

// 制作
void Map::makeClue() {
	for (int i = 0; i < 5; i++) {
		clue[0][i] = getClueCol(i);
		clue[1][i] = getClueRow(i);
	}
}

// 获取
string Map::getClue() {
	string buff = "";
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 5; j++) {
			buff += clue[i][j] + '/';
		}
	}
	return buff.substr(0, buff.size() - 1);
}

/*****************************************/

void dfs(int t, Map m, map<string, string>& bru) {
	if (t > 24) {
		string hashedSol = md5(m.getClue() + m.serialize());
		bru[hashedSol] = m.serialize();
		if (m.serialize() == "nyyyyynyyyynnnynnynnnnyny") {
			cout << "(" << m.getClue() << m.serialize() << ")" << endl;
			cout << hashedSol << endl;
		}
		return;
	}
	int i = t / 5;
	int j = t % 5;
	m.con[i][j] = true;
	m.setBlock(i, j, 1);
	dfs(t + 1, m, bru);
	m.setBlock(i, j, 0);
	dfs(t + 1, m, bru);
	m.con[i][j] = false;
}

Puzzle get5x5() {
	Puzzle puz;
	httplib::Client cli("https://cn.puzzle-nonograms.com");
	auto resp = cli.Get("/");
	if (resp->status != 200) {
		cout << "[ERR] http GET faild when getting puzzle, status code: " << resp->status << endl;
		return puz;
	}
	cout << "[LOG] http get successful." << endl;
	string ress = resp->body;
	string taskMark = "task = '";
	string hashMark = "hashedSolution: '";
	string paramMark = "name=\"param\" value=\"";
	int i = ress.find(taskMark);
	if (i >= ress.length()) {
		cout << "[ERR] no task found" << endl;
		return puz;
	}
	cout << "[LOG] task found" << endl;
	string buff = "";
	for (i += taskMark.size(); ress[i] != '\''; i++) {
		buff += ress[i];
	}
	puz.task = buff;
	i = ress.find(hashMark);
	if (i >= ress.length()) {
		cout << "[ERR] no hash found" << endl;
		return puz;
	}
	cout << "[LOG] hash found" << endl;
	buff = "";
	for (i += hashMark.size(); ress[i] != '\''; i++) {
		buff += ress[i];
	}
	puz.hash = buff;
	i = ress.find(paramMark);
	if (i >= ress.length()) {
		cout << "[ERR] no param found" << endl;
		return puz;
	}
	cout << "[LOG] param found" << endl;
	buff = "";
	for (i += paramMark.size(); ress[i] != '\"'; i++) {
		buff += ress[i];
	}
	puz.param = buff;
	return puz;
}

string submit(Puzzle p, string serialize) {
	httplib::Client cli("https://cn.puzzle-nonograms.com");
	httplib::Params params{
		{ "robot", "1" },
		{ "b", "1" },
		{ "size", "0" },
		{ "param", p.param },
		{ "w", "5" },
		{ "h", "5" },
		{ "ansH", serialize },
		{ "ready", "+++完成+++" }
	};
	auto res = cli.Post("/", params);
	if (res->status != 200) {
		cout << "[ERR] http POST faild when submitting, status code: " << res->status << endl;
		return "";
	}
	string ress = res->body;
	string solparamsMark = "name=\"solparams\" value=\"";
	int i = ress.find(solparamsMark);
	if (i > ress.size()) {
		cout << "[ERR] Wrong ans." << endl;
		return "";
	}
	cout << "[LOG] solparams found" << endl;
	string buff = "";
	for (i += solparamsMark.size(); ress[i] != '\"'; i++) {
		buff += ress[i];
	}
	return buff;
}

void hallSubmit(string solparams) {
	httplib::Client cli("https://cn.puzzle-nonograms.com");
	httplib::Params params{
		{ "submitscore", "1" },
		{ "solparams", solparams },
		{ "robot", "1" },
		{ "email", "h2so4@mhyc.eu.org" } // config your email here
	};
	auto res = cli.Post("/hallsubmit.php", params);
	if (res->status == 302) {
		cout << "[LOG] hall submit returned 302, It's OK." << endl;
	}
	else {
		cout << "[ERR] hall submit faild, status code: " << res->status << endl;
		cout << "[ERR] code 200 means network is OK but your submition isn't correct." << endl;
	}
}


int main() {
	map<string, string> bru;
	Map m;
	cout << md5("2/1/2.2/2/3.1/4/1.3/1.1/1/1.1nyyyyynyyyynnnynnynnnnyny");

	dfs(0, m, bru);

	/*m.setBlock(0, 1, 1);
	m.setBlock(2, 0, 1);
	m.setBlock(1, 0, 1);
	m.setBlock(0, 2, 1);
	m.setBlock(0, 3, 1);
	m.setBlock(0, 4, 1);
	m.setBlock(1, 2, 1);
	m.setBlock(1, 3, 1);
	m.setBlock(1, 4, 1);
	m.setBlock(2, 4, 1);
	m.setBlock(3, 2, 1);
	m.setBlock(4, 2, 1);
	m.setBlock(4, 4, 1);

	cout << m.serialize() << endl;
	cout << m.check("2/1/2.2/2/3.1/4/1.3/1.1/1/1.1") << endl;

	cout << hasher.encode(m.getClue() + m.serialize()) << endl;*/

	Puzzle p = get5x5();
	cout << p.task << endl;
	cout << p.hash << endl;
	cout << p.param << endl;




	cout << bru[p.hash] << endl;
	string solparam = submit(p, bru[p.hash]);
	hallSubmit(solparam);
}



